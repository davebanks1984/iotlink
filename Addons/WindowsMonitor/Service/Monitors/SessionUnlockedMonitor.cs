﻿using IOTLinkAPI.Configs;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform.HomeAssistant;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace IOTLinkAddon.Service.Monitors
{
    class SessionUnlockedMonitor : BaseMonitor
    {
        private static readonly string CONFIG_KEY = "SystemInfo";

        private SessionStateService _service;

        public SessionUnlockedMonitor(SessionStateService sessionStateService)
        {
            this._service = sessionStateService;
        }

        public override string GetConfigKey()
        {
            return CONFIG_KEY;
        }

        public override List<MonitorItem> GetMonitorItems(Configuration config, int interval)
        {
            List<MonitorItem> result = new List<MonitorItem>();

            // Boot Time
            result.Add(new MonitorItem
            {
                ConfigKey = CONFIG_KEY,
                Type = MonitorItemType.TYPE_RAW,
                Topic = "Stats/System/SessionUnlocked",
                Value = _service.GetCurrentUserUnlocked(),
                DiscoveryOptions = new HassDiscoveryOptions()
                {
                    Component = HomeAssistantComponent.BinarySensor,
                    Id = "SessionUnlocked",
                    Name = "Session Unlocked",
                    PayloadOff = "False",
                    PayloadOn = "True",
                    DeviceClass = "lock"
                }
            }); ; ;

            return result;
        }
    }
}
